# Yetistack Core

This stack is the central structure for all of the other Minecraft related services and components.  The Core supplies a 
central game proxy that allows multiple game servers to communicate and share player connections as well 
as a database for shared storage.  A default "lobby" game server is included as well. 

This image uses the [Jenkins build of Waterfall](), "a drag and drop replacement for BungeeCord that adds new features, fixes bugs, and improves performance." For more information on the software itself check [the GitHub repo for it](https://github.com/WaterfallMC/Waterfall) or [its webpage over at the Aquifer website](https://aquifermc.org/pages/about-waterfall/). For information about the original BungeeCord check [the BungeeCord Wiki](https://www.spigotmc.org/wiki/about-bungeecord/).

This readme is temporary and will be updated and added to soon, and continuously as documentation needs are determined.
