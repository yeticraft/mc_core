#!/bin/sh

WATERFALL_JAR=$WATERFALL_HOME/Waterfall.jar

if [[ ! -e $WATERFALL_JAR ]]; then
    echo "Downloading proxy software..."
    if ! curl -o $WATERFALL_JAR -fsSL https://ci.destroystokyo.com/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar; then
        echo "ERROR: failed to download" >&2
        exit 2
    fi
fi

chown -R mcproxy:mcproxy $WATERFALL_HOME

echo "Setting initial memory to ${INIT_MEMORY:-${MEMORY}} and max to ${MAX_MEMORY:-${MEMORY}}"
JVM_OPTS="-Xms${INIT_MEMORY:-${MEMORY}} -Xmx${MAX_MEMORY:-${MEMORY}} ${JVM_OPTS}"

exec sudo -u mcproxy java $JVM_OPTS -jar $WATERFALL_JAR "$@"
#exec sudo -u mcproxy java $JVM_OPTS -jar $WATERFALL_JAR $WATERFALL_ARGS
